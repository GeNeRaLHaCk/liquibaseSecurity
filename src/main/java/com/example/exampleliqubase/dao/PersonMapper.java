package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.PersonEntity;

public interface PersonMapper {
    int deleteByPrimaryKey(Long personId);

    int insert(PersonEntity record);

    int insertSelective(PersonEntity record);

    PersonEntity selectByPrimaryKey(Long personId);

    int updateByPrimaryKeySelective(PersonEntity record);

    int updateByPrimaryKey(PersonEntity record);
}